### Copyright 2016 Carlo A. Furia
### This software is distributed under the terms of
### the GNU General Public License version 3.


from collections import OrderedDict
import tb.thinkbayes as tb


class LangExperiments(object):

    # language -> group_key -> [record]
    data = None

    linenumber = "_linenumber"
    default_lang = "__"

    def __init__(self, group_key, language_key="language"):
        self.data = {}
        self.fieldnames = []
        self.languages = []
        self.group_key = group_key
        self.language_key = language_key

    def __iter__(self):
        """Iterator over tuples (language, group, record)"""
        self._it_lang = -1
        self._groups = []
        self._it_group = -1
        self._it_record = -1
        self._n_records = 0
        return self

    def next(self):
        self._it_record += 1
        if self._it_record >= self._n_records:
            self._it_record = 0
            self._it_group += 1
            if self._it_group >= len(self._groups):
                self._it_group = 0
                self._it_lang += 1
                if self._it_lang >= len(self.languages):
                    raise StopIteration
        lang = self.languages[self._it_lang]
        self._groups = self.data[lang].keys()
        group = self._groups[self._it_group]
        self._n_records = len(self.data[lang][group])
        record = self.data[lang][group][self._it_record]
        return (lang, group, record)

    def add(self, record, lang, group):
        """Add a record to the data for language 'lang' in group 'group'.

        record: a dictionary with keys a superset of self.fieldnames"""
        keys = [k for k in record] if hasattr(record, "__iter__") else self.fieldnames
        if set(keys) < set(self.fieldnames):
            missing = set(self.fieldnames) - set(keys)
            raise ValueError("Incomplete record: keys %s missing" % str(missing))
        if not self.data.has_key(lang):
            self.languages.append(lang)
            self.data[lang] = OrderedDict()
        try:
            self.data[lang][group].append(record)
        except KeyError:
            self.data[lang][group] = [record]

    def from_file(self, data_dir, filename, delimiter=":", language_convert=None):
        """Populate data from DSV file 'filename' in 'data_dir'."""
        import os, os.path
        import csv
        with open(os.path.join(data_dir, filename)) as fp:
            listreader = csv.reader(fp, delimiter=delimiter, skipinitialspace=True)
            self.fieldnames = [fp.strip() for fp in listreader.next()]
            for raw in listreader:
                record = {key: value \
                          for (key, value) in [(self.fieldnames[k], raw[k].strip()) \
                                               for k in range(len(self.fieldnames))]}
                # add line number in DSV file for traceability
                record[self.linenumber] = listreader.line_num
                group = record[self.group_key]
                if self.language_key is None:
                    lang = self.default_lang
                else:
                    lang = record[self.language_key]
                if language_convert is not None:
                    lang = language_convert(lang)
                self.add(record, lang, group)
            self.fieldnames += [self.linenumber]

    def map_content(self, key, mapf=lambda v: v):
        """Apply 'mapf' to all records at 'key'."""
        for language, group, record in self:
            record[key] = mapf(record[key])

    def extract(self, project,
                keep_group=lambda grp: True, keep_record=lambda rec: True):
        """Return only records that satisfy 'keep_record' 
           in groups that satisfy 'keep_group', and projects 
           them out on 'project'.

        project: a record's key or a projection function over records
        keep_group: a boolean filter predicate over group names
        keep_record: a boolean filter predicate over records
        """
        result = LangExperiments(self.group_key, language_key=self.language_key)
        if project in self.fieldnames:
            project_fun = lambda rec: rec[project]
        else:
            project_fun = project
        for lang, group, record in self:
            if keep_group(group) and keep_record(record):
                v = project_fun(record)
                result.add(v, lang, group)
        return result

    def pairup(self, aggregate):
        """Paired data aggregated using 'aggregate' by (shared) group.

        returns a LangComparisonData object
        aggregate: aggregation function that inputs a pair of lists 
                   (one per language) and returns a pair of values (one per language)
        """
        result = LangComparisonData(self.languages)
        for lang1, lang2 in result:
            groups1 = self.data[lang1].keys()
            groups2 = self.data[lang2].keys()
            shared_groups = sorted(list(set(groups1) & set(groups2)))
            for group in shared_groups:
                datalist1 = self.data[lang1][group]
                datalist2 = self.data[lang2][group]
                value1, value2 = aggregate(datalist1, datalist2)
                result[(lang1, lang2)].values.append((value1, value2))
            result[(lang1, lang2)].groups = shared_groups
        return result

        
class LangComparisonData(dict):

    class Comp(object):
        def __init__(self):
            self.values = []
            self.groups = []
            self.aggregated = False
            self.pmf = None
            self.suite = None
            self.posterior = None
            
    def __init__(self, languages, *args, **kwds):
        """Setup empty entries for all pairs in languages x languages."""
        super(LangComparisonData, self).__init__(*args, **kwds)
        self.languages = languages
        langpairs = [(lang1, lang2) \
                     for lang1 in self.languages for lang2 in self.languages \
                     if lang1 < lang2]
        for pair in langpairs:
            self[pair] = self.Comp()

    def aggregate(self, aggfun):
        """Aggregate 'values' in each group using 'aggfun'.

        aggfun: a function (value, value) -> value
        """
        for pair in self:
            aggdata = [aggfun(v1, v2) for v1, v2 in self[pair].values]
            self[pair].values = aggdata
            self[pair].aggregated = True

    def merge_groups(self):
        """Flatten groups in 'values' for all language pairs."""
        import operator
        for pair in self:
            self[pair].values = reduce(operator.add, self[pair].values, [])

    def combine(self, other, combfun):
        """Change every value in 'other' to combfun(self's value, other's value).

        other: a LangComparisonData object with groups equal to self's
        combfun: a function (value, value) -> value"""
        for pair in self:
            if pair in other:
                if self[pair].groups != other[pair].groups:
                    raise ValueError("Object 'other' has different groups for %s"
                                     % str(pair))
                v_self = self[pair].values
                v_other = other[pair].values
                assert len(v_self) == len(v_other)
                comb = [combfun(v_self[k], v_other[k]) for k in range(len(v_self))]
                other[pair].values = comb


class LanguageComparison(object):
    """Comparison based on Bayes analysis of one language pair."""

    import numpy

    n = 10000

    class DiffSuite(tb.Suite):
        """Suite with likelihood based on distribution of differences.
           Uses logarithmic transform to update prior 
           based on multiple observations."""
        import math
        
        def __init__(self, pmf, diffs, name=""):
            """
            pmf: prior distribution (Pmf object)
            diffs: list of differences (for variability model)
            """
            tb.Suite.__init__(self, pmf, name=name)
            self.diffs_pdf = tb.EstimatedPdf(diffs)
            
        def Likelihood(self, data, hypo):
            diff = data - hypo
            like = self.diffs_pdf.Density(diff)
            return like
        
        def LogLikelihood(self, data, hypo):
            like = self.Likelihood(data, hypo)
            if like:
                log_like = self.math.log(like)
            else:
                log_like = 0
            return log_like

    def _nospaces(self, ss):
        return "".join([s for s in ss if s not in [" ", "\t"]])
    
    def __init__(self, pair, values, diffs,
                 name_pre="",
                 impossible=lambda x: False,
                 low=None, high=None):
        """
        Comparison of languages 'pair' based on 'values' 
        and distribution of difference variabilities 'diffs'.

        pair: pair of language names
        values: list of values of the comparison between the two languages
        diffs: list of possible differences (for variability model)
        name_pre: prefix used before file name
        impossible: values that must have probability zero (after smoothing)
        low, high: lower and upper bounds of range (min and max if None)
        """
        self.lang1, self.lang2 = pair
        self.name = print_name(self.lang1) + "/" + print_name(self.lang2)
        self.name_pre = name_pre
        self.fname = self.name_pre + self._nospaces(self.lang1 + "-" + self.lang2)
        self.make_pmf(values, impossible=impossible, low=low, high=high)
        self.suite = self.DiffSuite(self.pmf, diffs, name=self.name)
        self.prior = None
        self.posterior = None

    def make_pmf(self, values,
                 impossible=lambda x: False,
                 low=None, high=None):
        """Create pmf for 'values' by smoothing, excluding 'impossible' values, 
           over range 'low'..'high'."""
        pdf = tb.EstimatedPdf(values)
        low = low if low else min(values)
        high = high if high else max(values)
        xs = self.numpy.linspace(low, high, self.n)
        pmf = pdf.MakePmf(xs)
        zeros = [x for x in xs if impossible(x)]
        if any(map(impossible, values)):
            raise ValueError("Impossible value present in distribution")
        for x in zeros:
            pmf.Remove(x)
        if zeros:
            pmf.Normalize()
        self.pmf = pmf

    def make_posterior(self, datas):
        """Update prior based on observations 'datas'."""
        if not self.posterior:
            self.prior = self.suite.Copy()
        else:
            self.prior = self.posterior
        self.posterior = self.prior.Copy(name=self.name + " (post)")
        if type(datas) is list:
            self.posterior.Log()
            self.posterior.LogUpdateSet(datas)
            self.posterior.Exp()
            self.posterior.Normalize()
        else:
            self.posterior.Update(datas)

    def make_stats(self):
        """Create standard stats of posterior distribution"""
        if self.posterior:
            self.stats = {}
            self.stats["mean"] = self.posterior.Mean()
            self.stats["median"] = tb.Percentile(self.posterior, 50)
            self.stats["CI-95"] = tb.CredibleInterval(self.posterior, 95)

    def plot(self, outdir=".", xlabel="values", ylabel="probability"):
        """Plot distribution of prior and posterior."""
        import tb.thinkplot as tp
        import os.path
        outname = os.path.join(outdir, self.fname)
        if self.suite and self.posterior:
            toplot = [self.suite, self.posterior]
            tp.Clf("qualitative")
            tp.PrePlot(num=len(toplot))
            tp.Pmfs(toplot)
            tp.Save(root=outname, formats=["pdf"], xlabel=xlabel, ylabel=ylabel)


def print_name(s):
    maps = {"C sharp": "C\\#",
            "F sharp": "F\\#"}
    if s in maps:
        return maps[s]
    else:
        return s

def todo_groups(data_dir, filename, feature_key, group_key="Task", \
                true_str="TRUE", delimiter=","):
    """List of groups such that field feature equals true_str.

    filename: DSV file
    group_key: name of field identifying groups
    feature_key: name of field identifying feature
    true_str: string in features that identifies groups to be included
    """
    exp = LangExperiments(group_key, language_key=None)
    exp.from_file(data_dir, filename, delimiter=delimiter)
    exp = exp.extract(feature_key,
                      keep_record=lambda rec: rec[feature_key] == true_str)
    assert len(exp.data) == 1
    for lang in exp.data:
        return exp.data[lang].keys()

def benchmark_agg_all(d1, d2):
    # each pair (x, y) in d1, d2 represents (input size, performance)
    v1 = map(lambda (x, y): (int(x), float(y)), d1)
    v2 = map(lambda (x, y): (int(x), float(y)), d2)
    # all pairs of solutions to the same input size
    pairs = [(p1, p2) for n1, p1 in v1 for n2, p2 in v2 if n1 == n2]
    return ([p1 for p1, p2 in pairs], [p2 for p1, p2 in pairs])

def benchmark_agg_min(d1, d2):
    # each pair (x, y) in d1, d2 represents (input size, performance)
    # compare the fastest solution for the smallest input size
    # that is available in both
    n1 = [n for n, s in map(lambda (x, y): (int(x), y), d1)]
    n2 = [n for n, s in map(lambda (x, y): (int(x), y), d2)]
    m = min(set(n1) & set(n2))
    m1 = [s for n, s in map(lambda (x, y): (int(x), float(y)), d1) if n == m]
    m2 = [s for n, s in map(lambda (x, y): (int(x), float(y)), d2) if n == m]
    return (min(m1), min(m2))

def rosetta_agg_min(d1, d2):
    # compare the fastest solution in each (minimum value)
    m1 = map(float, d1)
    m2 = map(float, d2)
    return (min(m1), min(m2))

def read_rosetta(data_dir):
    def bench_lang(lang):
        """Name conversion benchmarks -> rosetta"""
        conv =  {"Csharpcore": "C sharp",
                 "Python3": "Python",
                 "Ghc": "Haskell",
                 "Fsharp": "F sharp",
                 "Gcc": "C",
                 "Jruby": "Ruby"}
        result = lang.title()  # capitalize first letter
        try:
            return conv[result]
        except KeyError:
            return result
    # scalability experiments in Rosetta code
    rosetta = LangExperiments("task", language_key="language")
    rosetta.from_file(data_dir, "rosetta_runtime.dsv", delimiter=":")
    # list of languages
    languages = rosetta.languages
    # metrics: running time and maximum memory used
    metrics = ["runtime", "memory"]
    todo_scalability = todo_groups(data_dir, "todo.csv", "Scalability")
    rosetta_pairs = {}
    for metric in metrics:
        # names of running time (user) and maximum RAM used (maxram)
        # in rosetta data
        metr = "user" if metric == "runtime" else "maxram"
        # extract metric of programs that executed correctly
        # and belong to tasks selected for scalability
        # and have non-zero metric
        extr = rosetta.extract(metr,
                               keep_group=lambda grp: grp in todo_scalability,
                               keep_record=lambda rec: rec["timeout"] == "0" \
                               and float(rec[metr]) > 0.0)
        # pair up measures for the same task,
        # selecting the best (smallest) in each language per task
        pairs = extr.pairup(rosetta_agg_min)
        rosetta_pairs[metric] = pairs
    # benchmarks experiments
    # http://benchmarksgame.alioth.debian.org/
    bench = LangExperiments("name", language_key="lang")
    bench.from_file(data_dir, "u64q_bulkdata.csv", delimiter=",", \
                    language_convert=bench_lang)
    # restrict languages to the same of Rosetta
    bench.languages = languages
    bench_pairs = {}
    bench_var = {}
    for metric in metrics:
        # names of running time (elapsed(s)) and maximum RAM used (mem(KB))
        # in benchmark data
        metr = "elapsed(s)" if metric == "runtime" else "mem(KB)"
        # extract (input size, metric) of programs that executed correctly
        # and have non-zero metric
        extr = bench.extract(lambda rec: (rec["n"], rec[metr]),
                             keep_record=lambda rec: rec["status"] == "0" \
                             and float(rec[metr]) > 0.0)
        # pair up measures for the same task, selecting the best
        pairs = extr.pairup(benchmark_agg_min)
        bench_pairs[metric] = pairs
        # pair up measures for the same task, selecting all
        var = extr.pairup(benchmark_agg_all)
        bench_var[metric] = var
    return (metrics, rosetta_pairs, bench_pairs, bench_var)


def update_pairs(values_pairs, diffs_pairs, datas_pairs,
                 xlabel="values", ylabel="probability",
                 metric_name="",
                 plot=None,
                 impossible=lambda x: -1 < x <= 1):
    """Compute and plot posterior distribution, given prior and data list.

    values_pairs: values of prior distributions 
                  (LangComparisonData object, with one 'values' list per pair)
    diffs_pairs:  differences, used as values for likelihood distributions
                  (LangComparisonData object, with one 'values' list per pair)
    datas_pairs:  data, used to update the prior to the posterior
                  (LangComparisonData object, with one 'values' list per pair)
    plot: directory where to plot output (None to skip plotting)
    impossible: range of values that must be excluded from the smoothed distributions
    """
    result = {}
    for pair in values_pairs:
        values = values_pairs[pair].values
        diffs = diffs_pairs[pair].values
        comp = LanguageComparison(pair, values, diffs,
                                  name_pre=metric_name + "_",
                                  impossible=impossible)
        datas = datas_pairs[pair].values
        comp.make_posterior(datas)
        comp.make_stats()
        if plot:
            comp.plot(outdir=plot, xlabel=xlabel, ylabel=ylabel)
        result[pair] = comp
    return result


class AgileSuite(object):
    """Suite for projects (agile and structured) with possible outcomes 
       and different likelyhoods."""

    def _mean(self, a_tuple):
        return sum([k*a_tuple[k] for k in range(len(a_tuple))])

    def better(self, a_tuple):
        """Is a_tuple better than self.reference?
           Compares distributions by evaluating their means:
           better = higher mean
        """
        assert self.n == len(a_tuple)
        return self._mean(a_tuple) > self._mean(self.reference)

    def multinomial(self, xs, n, ps):
        """Multinomial pmf(x_1, ..., x_k; n, p_1, ..., p_k).
           Probability of drawing x_i balls of kind i, for 1 <= i <= k,
           for a total of n balls draws with replacement from a bag
           where the probability of drawing a ball of kind i is p_i.
        """
        from math import factorial as f
        from operator import mul
        assert len(ps) == len(xs) == self.n
        assert sum(xs) == n
        res = 1.0
        res *= 1.0*f(n)/reduce(mul, map(f, xs), 1)
        res *= reduce(mul, [ps[k]**xs[k] for k in range(len(xs))], 1)
        return res

    def weight(self, ps, model="uniform"):
        "Weight of probabilities, without normalization."
        if model == "uniform":
            # uniform: all weights are the same
            return 1.0
        elif model == "triangle":
            # scale linearly with distance from mean of reference
            m_ps = self._mean(ps)
            m_r = self._mean(self.reference)
            return 2.0 - abs(m_ps - m_r)
        elif model == "power":
            # scale as a power with distance from mean of reference
            m_ps = self._mean(ps)
            m_r = self._mean(self.reference)
            return 1.0/(1 + abs(m_ps - m_r))
        elif model == "exp":
            # scale exponentially with distance from mean of reference
            m_ps = self._mean(ps)
            m_r = self._mean(self.reference)
            return 10.0**(-abs(m_ps - m_r))

    def __init__(self, reference_probs, model="uniform"):
        from itertools import product as cartesian
        """
        reference_probs: list of probabilities for so many outcomes
        model: weights of different models, one of: uniform, triangle, power, exp
        """
        self.reference = reference_probs
        self.n = len(reference_probs)
        # Cartesian product [0..100]^n (or [0..10]^n for speed if n large)
        if self.n <= 3:
            upto = 100
        else:
            upto = 10
        probs = [ps for ps in cartesian(*(self.n*[range(0,upto+1)])) \
                 if sum(ps) == upto]
        probs = [map(lambda p: 1.0*p/upto, ps) for ps in probs]
        better = [ps for ps in probs if self.better(ps)]
        worse = [ps for ps in probs if not self.better(ps)]
        self.better = better
        self.worse = worse
        self.probs = better + worse
        self.weights_b = [self.weight(ps, model=model) for ps in better]
        self.weights_w = [self.weight(ps, model=model) for ps in worse]
        self.weights = [self.weight(ps, model=model) for ps in self.probs]
        # normalize weights
        for wgs in [self.weights_b, self.weights_w, self.weights]:
            wgs = [w/sum(wgs) for w in wgs]

    def collect_prob(self, xs, n, probs, weights):
        """Cumulative probability of xs given probs and weights."""
        like = 0.0
        for k in range(len(probs)):
            ps = probs[k]
            w = weights[k]
            p = self.multinomial(xs, n, ps)
            like += w*p
        return like

    def Likelihood(self, data, hypo):
        """
        data: an outcome or list of outcomes
        hypo: one of "agile better" and "no difference"
        """
        data = data if type(data) is list else [data]
        struc = [d for k, d in data if k == "structured"]
        xs_s = self.n*[0]
        for o in struc:
            xs_s[o] += 1
        agile = [d for k, d in data if k == "agile"]
        xs_a = self.n*[0]
        for o in agile:
            xs_a[o] += 1
        if hypo == "no difference":
            # probabilities can be anything
            struc_like = self.collect_prob(xs_s, len(struc), self.probs, self.weights)
            agile_like = self.collect_prob(xs_a, len(agile), self.probs, self.weights)
        elif hypo == "agile better":
            # probabilities are worse (no better) than reference for structured
            struc_like = self.collect_prob(xs_s, len(struc),
                                           self.worse, self.weights_w)
            # probabilities are better than reference for agile
            agile_like = self.collect_prob(xs_a, len(agile),
                                           self.better, self.weights_b)
        like = agile_like * struc_like
        return like


def percent_to_ordinal(r):
    if r == "None":
        return 0
    elif r == "1-10%":
        return 1
    elif r == "11-20%":
        return 2
    elif r == "21-30%":
        return 3
    elif r == "31-40%":
        return 4
    elif r == "41-50%":
        return 5
    elif r == "51-60%":
        return 6
    elif r == "61-70%":
        return 7
    elif r == "71-80%":
        return 8
    elif r == "81-90%":
        return 9
    elif r == "91-100%":
        return 10
    elif r == "Don't Know":
        return -1
    elif r == "":
        return -2
    else:
        raise ValueError("Unrecognized value range: " + r)

def effective_to_ordinal(r):
    if r == "Very Ineffective":
        return 0
    elif r == "Ineffective":
        return 1
    elif r == "Neutral":
        return 2
    elif r == "Effective":
        return 3
    elif r == "Very Effective":
        return 4
    elif r == "Not Applicable":
        return -1
    elif r == "":
        return -2
    else:
        raise ValueError("Unrecognized value range: " + r)

def tenrange_to_ordinal(v, r, R, low, high):
    "Rescale the value v in range r--R into an integer between low and high"
    import numpy
    assert r <= R
    if v < r:
        v_prime = r
    elif R < v:
        v_prime = R
    else:
        v_prime = v
    assert r <= v_prime <= R
    arng = numpy.linspace(r, R, high - low + 1)
    res = abs(arng - v_prime).argmin()
    assert low <= res <= high
    return res

def range_to_ordinal(r, low, high, step, M=3):
    assert low <= r <= high
    h = high
    result = M - 1
    while low < h:
        if h - step < r <= h:
            return result
        result -= 1
        h -= step
    return result
    
def prob_of_success(r):
    dist = (r["failure"], r["challenge"], r["success"])
    return tuple([1.0*x/sum(dist) for x in dist]) 

def merge_prob(vec, prob=True):
    """Merge list 'vec' of probability tuples for n outcomes"""
    npos = len(vec[0])
    res = []
    for n in range(npos):
        wvals = [1.0*vec[k][n]/len(vec) for k in range(len(vec))]
        res.append(sum(wvals))
    if prob:
        return tuple([(k, res[k]) for k in range(len(res))])
    else:
        # return frequencies
        return tuple([(k, round(100*res[k])) for k in range(len(res))])

def read_agile(data_dir, key, low=1, high=10, agile_is="AL", struc_is="TI",
               g="0"):
    """Read data about 'key' scaled according to low, high range.

    key: outcome or stakeholders
    low: lower bound on the data read
    high: upper bound on the data read
    agile_is: string of characters in ALTI, for Agile, Lean, Traditional, Iterative
    """
    # "languages" are the development methodology used in each project
    ### data from our survey
    survey = LangExperiments("group", language_key="type")
    survey.from_file(data_dir, "survey.csv", delimiter=",")
    survey.map_content(key, lambda v: int(v))
    # translate to 0--2 range or 0--4 range
    _low = 0
    _high = 2 if key in ["outcome"] else 4
    survey = survey.extract(lambda r: tenrange_to_ordinal(r[key], \
                                                          low, high, _low, _high))
    # agile and structured data, plain
    survey = {"agile": [x for x in survey.data["Agile"][g]],
              "structured": [x for x in survey.data["Structured"][g]]}
    ### data from IT project survey
    itproj = LangExperiments("group", language_key="type")
    itproj.from_file(data_dir, "itproj.csv", delimiter=",")
    for k in ["percent", "success", "challenge", "failure"]:
        itproj.map_content(k, percent_to_ordinal)
    for k in ["time", "ROI", "stakeholders", "quality"]:
        itproj.map_content(k, effective_to_ordinal)
    # projects for which meaningful data is available:
    # percent > 0: at least some projects of that category were done
    # success >= 0, challenge >= 0, failure >= 0:
    #             data about outcome is available (no Don't Knows)
    # success + challenge + failure > 7:
    #    it should actually sum up to 10 (percentages of the same projects)
    #    but as long as it's at least 8 we can still normalize the data
    repOK = lambda rec: rec["percent"] > 0 and rec["success"] >= 0 \
            and rec["challenge"] >= 0 and rec["failure"] >= 0 and \
            sum([rec["success"], rec["challenge"], rec["failure"]]) > 7
    if key in ["outcome"]:
        # prob_of_success does the rescaling to 1
        itproj = itproj.extract(prob_of_success, keep_record=repOK)
    else:
        # other dataset: stakeholder value brought by project
        itproj = itproj.extract("stakeholders", 
                                keep_record=lambda r: r["stakeholders"] > -1)
    agile = []
    for t in agile_is:
        agile += itproj.data[t][g]
    struc = []
    for t in struc_is:
        struc += itproj.data[t][g]
    if key in ["outcome"]:
        # change probabilities into sequence of values
        agile = [int(round(10*p*len(agile)))*[k] for k, p in merge_prob(agile)]
        struc = [int(round(10*p*len(struc)))*[k] for k, p in merge_prob(struc)]
        # flatten
        agile = [item for sublist in agile for item in sublist]
        struc = [item for sublist in struc for item in sublist]            
    itproj = {"agile": agile,
              "structured": struc}
    return (itproj, survey)


class Pareto(tb.Pdf):
    """PDF for Pareto distribution"""

    import numpy

    def __init__(self, a, b, zero=0):
        """Pareto distribution with parameters 
           a (also alpha, shape) and b (also x_m, scale)

        zero: shifting value for evaluation (to avoid zeros)
        """
        self.a = a
        self.b = b
        self.zero = zero

    def Density(self, x):
        x_prime = x + self.zero
        if x_prime < self.b:
            res = 0.0
        else:
            res = (1.0 * self.a * self.b**self.a)/(x**(self.a + 1))
        return res


class Weibull(tb.Pdf):
    """PDF for Weibull distribution"""

    import numpy

    def __init__(self, alpha, beta, zero=0, name=""):
        """Weibull distribution with parameters 
           alpha (also lambda, scale) and beta (also k, shape)

        zero: shifting value for evaluation (to avoid zeros)
        """
        self.alpha = alpha
        self.beta = beta
        self.zero = zero
        self.name = name

    def Density(self, x):
        import math
        x_prime = x + self.zero
        if x_prime < 0:
            res = 0.0
        else:
            res = (1.0*self.beta/self.alpha) \
                * (1.0*x_prime/self.alpha)**(self.beta - 1) \
                * math.exp(-1.0*((1.0*x_prime/self.alpha)**self.beta))
        return res


class TwoParmsEstimate(tb.Suite, tb.Joint):

    import numpy

    def init_uniform(self):
        """Initialize prior using uniform distribution."""
        tb.Suite.__init__(self, self.pairs, name=self.name)

    def init_triangle(self):
        """Initialize prior using triangle distribution."""
        def p_triang(x, c, a, b):
            if a <= x < c:
                res = 2.0*(x - a)/((b - a)*(c - a))
            elif x == c:
                res = 2.0/(b - a)
            elif c < x <= b:
                res = 2.0*(b - x)/((b - a)*(b - c))
            else:
                res = 0
            return res
        ps = [((a, b),
               p_triang(a, self.nom_alpha, self.low_alpha, self.high_alpha) \
               * p_triang(b, self.nom_beta, self.low_beta, self.high_beta)) \
               for a, b in self.pairs]
        pmf = tb.MakePmfFromDict(dict(ps))
        pmf.Normalize()
        tb.Suite.__init__(self, pmf, name=self.name)

    def init_jeffreys(self):
        """Initialize prior using Jeffreys prior."""
        ps = [((a, b), 1.0/(a*b)**2) for a, b in self.pairs]
        pmf = tb.MakePmfFromDict(dict(ps))
        pmf.Normalize()
        tb.Suite.__init__(self, pmf, name=self.name)

    def __init__(self,
                 alpha, alpha_range,
                 beta, beta_range,
                 nsamples = 200, 
                 prior=None,
                 name=""):
        """
        alpha, beta: nominal value for parameters alpha, beta
        alpha_range: (min, max) variability range for estimated alphas
        beta_range: (min, max) variability range for estimated betas
        prior: name of prior to initialize
        """
        self.nom_alpha = alpha
        self.nom_beta = beta
        self.low_alpha, self.high_alpha = alpha_range
        self.low_beta, self.high_beta = beta_range
        alphas = self.numpy.linspace(self.low_alpha, alpha, nsamples) \
                 + self.numpy.linspace(alpha, self.high_alpha, nsamples+1)[1:]
        betas = self.numpy.linspace(self.low_beta, beta, nsamples) \
                 + self.numpy.linspace(beta, self.high_beta, nsamples+1)[1:]
        self.pairs = [(a, b) for a in alphas for b in betas]
        self.name = name
        if prior in ["uniform"]:
            self.init_uniform()
        elif prior in ["triangle"]:
            self.init_triangle()
        elif prior in ["jeffreys"]:
            self.init_jeffreys()


class WeibullEstimate(TwoParmsEstimate):

    def Likelihood(self, data, hypo):
        alpha, beta = hypo
        t = data
        pdf = Weibull(alpha, beta, zero=1)
        like = pdf.Density(t)
        return like

class ParetoEstimate(TwoParmsEstimate):

    def Likelihood(self, data, hypo):
        import math
        a, b = hypo
        t = data
        pdf = Pareto(a, b, zero=math.floor(b)+1)
        like = pdf.Density(t)
        return like

    def LogLikelihood(self, data, hypo):
        like = self.Likelihood(data, hypo)
        if like:
            log_like = self.math.log(like)
        else:
            log_like = 0
        return log_like


class TwoParamsDerived(object):

    import numpy

    def __init__(self, params_pmf, pdf_class,
                 alpha_name="alpha", beta_name="beta"):
        """
        params_pmf: suite of distribution with parameters alpha, beta
        pdf_class: class object for derived distributions
        alpha_name: actual name given to parameter alpha
        beta_name: actual name given to parameter beta
        """
        self.params_pmf = params_pmf
        self.pdf_class = pdf_class
        self.alpha_name = alpha_name
        self.beta_name = beta_name

    def ratios(self,
               fun,
               credibility=95,
               n=100,
               name=""):
        """Distribution of fun for alpha, beta ranging over credibility intervals

        fun: function of one argument (a pdf)
        credibility: confidence level 0-100 for credibility intervals, 
                     defining range of alpha, beta
        n: number of intermediate alpha, beta values (for discretization)
        """
        marginal_alpha = self.params_pmf.Marginal(0, name="alpha")
        marginal_beta = self.params_pmf.Marginal(1, name="beta")
        alpha_rng = marginal_alpha.CredibleInterval(credibility)
        alpha_rng = self.numpy.linspace(alpha_rng[0], alpha_rng[1], n)
        beta_rng = marginal_beta.CredibleInterval(credibility)
        beta_rng = self.numpy.linspace(beta_rng[0], beta_rng[1], n)
        pairs = [(alpha, beta) for alpha in alpha_rng for beta in beta_rng]
        probs = []
        for alpha, beta in pairs:
            pdf = self.pdf_class(alpha, beta)
            prob = fun(pdf)
            probs.append(prob)
        assert len(probs) == len(pairs)
        probs = [(pairs[k], probs[k]) for k in range(len(probs))]
        pmf = tb.MakePmfFromDict(dict(probs))
        return tb.Joint(pmf, name=name)


class Tester(tb.Suite):

    def __init__(self, alpha, beta, E, e, xs, zero=0):
        """
        alpha, beta: parameters of prior Weibull
        E: fraction of bugs detected in prior distribution
        e: probability tester finds a bug (in posterior)
        xs: range of values to discretize Weibull
        zero: zero shifting of Weibull
        """
        pdf = Weibull(alpha, beta, zero=zero)
        name = "$\\alpha = %f, \\beta = %f$" % \
                           (round(alpha, 2), round(beta, 2))
        pmf = pdf.MakePmf(xs)
        pmf = pmf.Scale(1.0/E)
        self.prior = pmf.Copy(name=name)
        tb.Suite.__init__(self, pmf)
        self.alpha = alpha
        self.beta = beta
        self.E = E
        self.e = e

    def set_e(self, e):
        self.e = e

    def Likelihood(self, data, hypo):
        t = data # number of bugs found by testing
        n = hypo # number of total bugs
        # given n bugs and e% detection rate for each of them,
        # probability of finding t bugs is given by the binomial B(n, e)[t]
        return tb.EvalBinomialPmf(t, n, self.e)


class MetaTester(tb.Suite):
    """Suite of Tester suites."""

    def __init__(self, Es, es,
                 alpha, beta, xs, zero=0):
        testers = [Tester(alpha, beta, E, e, xs, zero=zero) for E in Es for e in es]
        tb.Suite.__init__(self, testers)

    def Likelihood(self, data, hypo):
        t = data # number of bugs found by testing
        tester = hypo
        like = tester.Update(t)
        return like

    def params_distro(self):
        items = [((tester.e, tester.E), prob) for tester, prob in self.Items()]
        pmf = tb.MakePmfFromItems(items)
        return tb.Joint(pmf)

    def nbugs_distro(self):
        return tb.MakeMixture(self)

    
def read_mbc(data_dir):
    import numpy
    # "languages" is whether the testing was done with original or MBC specs
    testing_d = LangExperiments("module", language_key="kind")
    testing_d.from_file(data_dir, "testing_all.csv", delimiter=",")
    testing_d.map_content("faults", lambda x: int(x))
    testing_d.map_content("LOC", lambda x: int(x))
    testing_d.map_content("routines", lambda x: int(x))
    # the tested modules (classes) are the same in both sets
    assert testing_d.data["original"].keys() == testing_d.data["mbc"].keys()
    # number of faults found in each module
    testing = testing_d.extract("faults")
    # number of routines in each module
    routines = testing_d.extract("routines")
    # LOC in each module
    LOC = testing_d.extract("LOC")
    # flatten lists, so as to have just a list of fault numbers
    original = [f[0] for c, f in testing.data["original"].iteritems()]
    mbc = [f[0] for c, f in testing.data["mbc"].iteritems()]
    routines = [f[0] for c, f in routines.data["original"].iteritems()]
    LOC = [f[0] for c, f in LOC.data["original"].iteritems()]
    # print "Total number of bugs: original %d, MBC %d" % (sum(original), sum(mbc))
    if round(numpy.mean(original), 4) != round(5.0952380952380949, 4) \
       or round(numpy.var(original), 4) != round(56.086167800453516, 4):
        raise \
            ValueError("Built ins are not set for original mean %f, variance %f" % \
                       (numpy.mean(original), numpy.var(original)))
    # To get an idea of parameters range for Weibull:
    # solve x * Gamma[1 + 1/y] = 5.0952380952380949,
    #       x^2 * (Gamma[1 + 2/y] - (Gamma[1 + 1/y])^2) = 56.086167800453516
    # for x = alpha, y = beta
    # where  5.09... = numpy.mean(original)
    #       56.08... = numpy.var(original)
    # and similar ones for the other cases
    alpha_original, beta_original = 4.00834, 0.696933
    alpha_mbc, beta_mbc = 5.72668, 0.711091
    alpha_both, beta_both = 4.79306, 0.693835
    init_weibull = Weibull(alpha_original, beta_original, zero=1)
    mbc_weibull = Weibull(alpha_mbc, beta_mbc, zero=1)
    # priors (suites, that is updatable)
    weibulls = {}
    a, b = alpha_original, beta_original
    a_low = a - (3*abs(alpha_mbc - alpha_original))
    a_high = a + (6*abs(alpha_mbc - alpha_original))
    b_low = 2*abs(beta_mbc - beta_original)
    b_high = b - (5*abs(beta_mbc - beta_original))
    for prior in ["uniform", "jeffreys"]:
        weibulls[prior] = WeibullEstimate(a, (a_low, a_high), b, (b_low, b_high),
                                          prior=prior, name=prior)
    return (original, mbc,
            {"weibull": (init_weibull, mbc_weibull, weibulls)},
            LOC, routines)
