# Bayesian Statistics in Software Engineering #

(Copyright (C) 2016  [Carlo A. Furia](http://bugcounting.net))

This repository contains material related to the paper:

**Bayesian Statistics in Software Engineering: Practical Guide and Case Studies**,
available at [arXiv.org 1608.06865](https://arxiv.org/abs/1608.06865)

The root directory contains the analysis scripts, which run using the
data available in the subdirectories.


### Contents ###

* `/`              the analysis scripts `an.py` and `collect.py`
* `agile/`         raw data for the _Analysis vs. Structured_ analysis
* `performance/`   raw data for the _Rosetta Code Language Comparison_ analysis
* `testing/`       raw data for the _Random Testing with Specifications_ analysis
* `tb/`            analysis code from the book [ThinkBayes](http://greenteapress.com/wp/think-bayes/) (used by the analysis scripts)

The `README.md` files in each subdirectory indicate the source of the content there.
