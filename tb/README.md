# Think Bayes #

This directory contains code from the book _Think Bayes_, by Allen B. Downey, available from [greenteapress.com](http://greenteapress.com/wp/think-bayes/).

The code is distributed under the GNU GPLv3 original license (see copyright notice at the top of each file); the original versions are [available on GitHub](https://github.com/AllenDowney/ThinkBayes).
