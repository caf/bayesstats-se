## Directory content and sources ###

#### `testing_all.csv`

Data about random testing of object-oriented Eiffel code with specifications from the paper:

Nadia Polikarpova, Carlo A. Furia, Yu Pei, Yi Wei, and Bertrand Meyer: _[What Good Are Strong Specifications?](http://bugcounting.net/publications.html#icse13)_, in Proceedings of the 35th International Conference on Software Engineering (ICSE), pgg. 257-266, ACM, 2013.
