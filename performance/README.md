## Directory content and sources ###

#### `rosetta_runtime.dsv` and `todo.csv`

Data about comparison of programming languages from the paper:

Sebastian Nanz and Carlo A. Furia: _[A Comparative Study of Programming Languages in Rosetta Code](http://bugcounting.net/publications.html#icse15)_. in Proceedings of the 37th International Conference on Software Engineering (ICSE), pgg. 778-788, ACM, 2015.


#### `u64q_bulkdata.csv`

Data about programming language performance from:

_[The Computer Language Benchmarks Game](http://benchmarksgame.alioth.debian.org/)_, (retrieved in August 2016)
