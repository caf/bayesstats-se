#! /usr/bin/python

### Copyright 2016 Carlo A. Furia
### This software is distributed under the terms of
### the GNU General Public License version 3.


import argparse
import collect
import os, os.path
import cPickle as pickle
from collections import OrderedDict

import tb.thinkplot as tp
import tb.thinkbayes as tb
import matplotlib.pyplot as pp
import numpy

available_analyses = ["rosetta", "agile", "testing", "ALL"]
data_dirs = {"rosetta": "performance", "agile": "agile", "testing": "testing"}


def speed_normalize(x, y):
    assert x > 0.0 and y > 0.0
    v1, v2 = (x, y)
    minv = v1 if v1 < v2 else v2
    maxv = v2 if v1 < v2 else v1
    # smaller is better
    sgn = -1 if v1 <= v2 else 1
    return sgn * (maxv / minv)

def rosetta_R(data):
    """Prepare data in format suitable for R"""
    graph_data = {}
    tab_data = {}
    median = data["median"]
    mean = data["mean"]
    assert not (-1 < median < 1)
    graph_data["mean"] = mean
    low, high = data["CI-95"]
    tab_data["CI"] = "(" +\
                     str(round(low, 1)) + ", " + str(round(high, 1)) +\
                     ")"
    sig = not ((low < 0 < high) or (-1.1 < mean < 1.1))
    delta = high - low
    smaller = min(abs(high), abs(low))
    higher = max(abs(high), abs(low))
    assert delta >= 0
    assert smaller > 0
    strongsig = sig and (delta < smaller or smaller > 2 or abs(median) > 2)
    weaksig = sig and not strongsig
    assert (not sig) or weaksig or strongsig
    if not sig:
        tab_data["CI"] = "\\notsig{%s}" % tab_data["CI"]
    elif weaksig:
        tab_data["CI"] = "\\weaksig{%s}" % tab_data["CI"]
    elif strongsig:
        tab_data["CI"] = "\\strongsig{%s}" % tab_data["CI"]
    tab_data["$\\mu$"] = round(data["mean"], 2)
    tab_data["$m$"] = round(data["median"], 2)
    draw = sig
    graph_data["draw"] = "true" if draw else "false"
    signif = 0
    if weaksig:
        signif = 1
    if strongsig:
        signif = 2
    graph_data["signif"] = signif
    effect = signif
    graph_data["effect"] = effect
    aux = 0
    if signif > 0 and effect > 0:
        aux += 1
    if signif > 1 or effect > 1:
        aux += 1
    graph_data["aux"] = aux
    return (graph_data, tab_data)


def rosetta_analysis(out_dir, scratch=False, rosetta_summary="rosetta_analysis.p"):
    metrics, rosetta_pairs, bench_pairs, bench_var = collect.read_rosetta(out_dir)
    for metric in metrics:
        # aggregate pairs to computer vector r
        rosetta_pairs[metric].aggregate(speed_normalize)
        bench_pairs[metric].aggregate(speed_normalize)
        # ratios only between inputs of same size
        bench_var[metric].aggregate(lambda s1, s2:
                                    [speed_normalize(s1[k], s2[k]) \
                                     for k in range(len(s1))])
        # variability = difference between a ratio
        # and any other ratio for the same task
        # note: 'combine' modifies bench_variability, not bench_pairs!
        bench_pairs[metric].combine(bench_var[metric],
                                    lambda s, o: [-float(s) + float(x) for x in o])
        # flatten so as to have one variability list per group/task
        bench_var[metric].merge_groups()
    fname = os.path.join(out_dir, rosetta_summary)
    if os.path.isfile(fname) and scratch:
        os.remove(fname)
    if os.path.isfile(fname):
        comps = pickle.load(open(fname, "r"))
    else:
        comps = {}
        for metric in metrics:
            cmps = collect.update_pairs(bench_pairs[metric],
                                        bench_var[metric],
                                        rosetta_pairs[metric],
                                        xlabel=metric + " ratio",
                                        ylabel="probability",
                                        metric_name=metric,
                                        plot=out_dir)
            comps[metric] = {pair: cmps[pair].stats for pair in cmps}
        pickle.dump(comps, open(fname, "w"))
    dmetr = metrics[0]
    languages = set([lang1 for lang1, lang2 in rosetta_pairs[dmetr].keys()] \
                    + [lang2 for lang1, lang2 in rosetta_pairs[dmetr].keys()])
    # data in R format
    import json
    for metric in metrics:
        R_graph = {}
        R_tab = {}
        for lang1 in languages:
            R_graph[lang1] = {}
            R_tab[lang1] = {}
            for lang2 in languages:
                if lang1 < lang2:
                    graph_data, tab_data = rosetta_R(comps[metric][(lang1, lang2)])
                    R_graph[lang1][lang2] = graph_data
                    R_tab[lang1][lang2] = tab_data
        fname = os.path.join(out_dir, "network_" + metric + ".json")
        json.dump(R_graph, open(fname, "w"))
        fname = os.path.join(out_dir, "tab_" + metric + ".json")
        json.dump(R_tab, open(fname, "w"))


def display_probs(ps):
    """Format probabilities ps"""
    assert abs(sum(ps) - 1.0) < 0.01
    # largest remainder method
    ps = map(lambda p: 1.0*100*p, ps)
    cs = map(lambda p: int(p), ps)
    gap = 100 - sum(cs)
    rems = sorted([(ps[k] - cs[k], k) for k in range(len(ps))])[::-1]
    assert gap <= len(rems)
    # add from largest reminder down
    for r, k in rems:
        if gap > 0:
            cs[k] += 1
            gap -= 1
    assert sum(cs) == 100
    return cs

def write_table(rows, outdir, fname):
    rows = ["  &  ".join(row) for row in rows]
    krems = []
    for k in range(len(rows)):
        if rows[k] == "\\hline" and k + 1 < len(rows):
            krems.append(k)
            rows[k + 1] = "\\hline\n" + rows[k + 1]
    rows = [rows[k] for k in range(len(rows)) if k not in krems]
    table = " \\\\\n".join(rows) + " \\\\"
    with open(os.path.join(outdir, fname), "w") as fp:
        fp.write(table)
    return table


def agile_analysis(outdir, sci_notation=False, print_info=False):
    agile_distros = ["", "A", "AL", "ALI"]
    struc_distros = ["", "T", "TI"]
    distros = sorted(list(set(["".join(list(set(a+s))) \
                               for a in agile_distros \
                               for s in struc_distros \
                               if a != "" or s != ""])))
    lows = [1, 3, 4, 5]
    for key in ["outcome", "stakeholders"]:
        if key == "outcome":
            n = 3
        else:
            n = 5
        probs = OrderedDict()
        rows = []
        for low in lows:
            for model in ["uniform", "triangle", "power", "exp"]:
                row = [model]
                for distro in distros:
                    # using all projects in one category, since I will merge anyway
                    kwargs = {"low": low, "agile_is": distro, "struc_is": "T"}
                    itproj, survey = collect.read_agile(outdir, key, **kwargs)
                    overall = itproj["agile"]
                    if print_info:
                        print "== %s ==" % key
                        print "distro: %s, low: %d" % (distro, low)
                    overall_pmf = tb.MakePmfFromList(overall, name="all")
                    overall_d = overall_pmf.Probs(range(n))
                    if print_info:
                        print "overall probs:", overall_d
                    probs[distro] = display_probs(overall_d)
                    n_probs = len(overall_d)
                    projects = collect.AgileSuite(overall_d, model=model)
                    # likelihoods
                    agile_data = [("agile", d) for d in survey["agile"]]
                    struc_data = [("structured", d) for d in survey["structured"]]
                    data = agile_data + struc_data
                    like_no_difference = projects.Likelihood(data, "no difference")
                    like_agile_better = projects.Likelihood(data, "agile better")
                    factor = like_agile_better / like_no_difference
                    if print_info:
                        print "Bayes factor:", factor
                    if sci_notation:
                        sfac = str("%.3E" % factor)
                    else:
                        sfac = str(round(factor, 4))
                        mis = 2+4 - len(sfac)
                        if 0 < mis <= (2+4-3):
                            sfac += mis*"0"
                    row.append(sfac)
                rows.append(row)
            if low != lows[-1]:
                rows.append(["\\hline"])
        write_table(rows, outdir, "tab_" + key + ".tex")
        rows = []
        for k in range(n_probs):
            row = ["$%s_{C}[{%d}]$" % \
                   ("\\outcome" if key == "outcome" else "\\stakeholders", k)]
            for distro in probs:
                row.append(str(probs[distro][k]) + "{\\scriptsize\,\\%}")
            rows.append(row)
        write_table(rows, outdir, "tab_" + key + "_distros.tex")


def credibility_intervals(suite, outname, xlabel="", ylabel="", formats=["pdf"]):
    """Make a plot showing several two-dimensional credibility intervals.
       Adapted from Think Bayes (paintball.py)
    """
    d = dict((pair, 0) for pair in suite.Values())
    percentages = [90, 75, 50, 25]
    for p in percentages:
        interval = suite.MaxLikeInterval(p)
        for pair in interval:
            d[pair] += 1
    tp.Contour(d, colorscheme="BuGn", contour=False, pcolor=True)
    tp.Save(outname,
            xlabel=xlabel, ylabel=ylabel, formats=formats)


def fun_prob_ratios(pdf, data_old, data_new):
    prob_new = 1.0 * sum([pdf.Density(d) for d in data_new]) / len(data_new)
    prob_old = 1.0 * sum([pdf.Density(d) for d in data_old]) / len(data_old)
    return prob_new / prob_old

def fun_lessequal_errors(pdf, errors):
    xs = numpy.linspace(0.1, 110, 100)
    pmf = pdf.MakePmf(xs)
    cdf = pmf.MakeCdf()
    return cdf.Prob(errors + 1)


def add_confidence(fig, low, high, opt, vopt, conf, name, sx=0.01, sy=0.0278):
    """Add confidence intervals (credibility) and maximum likelihood markers to fig."""
    x, X = pp.xlim()
    ux = (X - x)*sx
    y, Y = pp.ylim()
    uy = (Y - y)*sy
    ax = fig.add_subplot(1, 1, 1)
    ax.annotate("$\widehat{%s} = %.2f$" % (name, round(opt, 2)),
                xy=(opt, vopt),
                xytext=(opt + 4*ux, vopt),
                arrowprops={"arrowstyle": "->"})
    hy = vopt/2
    ax.annotate("",
                xy=(high, vopt/2),
                xytext=(low, hy),
                arrowprops={"arrowstyle": "|-|"})
    ax.annotate("$%s_{%d}^h = %.2f$" % (name, conf, round(high, 2)),
                xy=(low, vopt/2),
                xytext=(high, vopt/2))
    lx, ly = low - 12*ux, vopt/2 + 1.5*uy
    if lx <= x:
        lx = x + ux
        ly = ly - 5*uy
    ax.annotate("$%s_{%d}^l = %.2f$" % (name, conf, round(low, 2)),
                xy=(high, vopt/2),
                xytext=(lx, ly))
    

def marginal_analysis(suite, prior_kind, confidence=95, 
                      outdir="testing/", plot=True):
    """Plot marginals of suite and computes statistics at confidence.
       Return (low, max_likelihood, high) of confidence% credible interval.

    suite: a bivariate distribution
    prior_kind: a string identifying the prior (for filenames)
    confidence: percentage 0-100 for credible interval
    outdir: where to write plots
    plot: if False, only compute statistics
    """
    marginal_alpha = suite.Marginal(0, name="$\\alpha$")
    marginal_beta = suite.Marginal(1, name="$\\beta$")
    alpha_low, alpha_high = marginal_alpha.CredibleInterval(confidence)
    beta_low, beta_high = marginal_beta.CredibleInterval(confidence)
    print "Alpha: mean %.2f, median %.2f" % (marginal_alpha.Mean(),
                                             tb.Percentile(marginal_alpha, 50))
    print "Beta: mean %.2f, median %.2f" % (marginal_beta.Mean(),
                                            tb.Percentile(marginal_beta, 50))
    alpha_opt = marginal_alpha.MaximumLikelihood()
    valpha_opt = marginal_alpha.Prob(alpha_opt)
    beta_opt = marginal_beta.MaximumLikelihood()
    vbeta_opt = marginal_beta.Prob(beta_opt)
    if plot:
        pmfs = [marginal_alpha, marginal_beta]
        fig = tp.Clf("qualitative")
        tp.PrePlot(num=len(pmfs))
        tp.Pmfs(pmfs)
        add_confidence(fig,
                       alpha_low, alpha_high,
                       alpha_opt, valpha_opt,
                       confidence, "\\alpha")
        add_confidence(fig,
                       beta_low, beta_high,
                       beta_opt, vbeta_opt,
                       confidence, "\\beta")
        fname = os.path.join(outdir, "marginals_" + prior_kind)
        tp.Save(root=fname, formats=["pdf"],
                xlabel="values of parameters $\\alpha, \\beta$", ylabel="probability")
    return ((alpha_low, alpha_opt, alpha_high),
            (beta_low, beta_opt, beta_high))


def derived_analysis(suite, n_errors, prior_kind, confidence=95,
                     outdir="testing/", plot=True):
    """Plot marginals of pmf derived from suite showing variability of n_errors
       with respect to parameters alpha, beta.

    suite: a bivariate distribution of alpha, beta
    prior_kind: a string identifying the prior (for filenames)
    confidence: percentage 0-100 for credible interval
    outdir: where to write plots
    plot: if False, only compute statistics
    """
    derived = collect.TwoParamsDerived(suite, collect.Weibull,
                                       alpha_name="$\\alpha$",
                                       beta_name="$\\beta$")
    # probability that a module has n_errors errors
    prob = derived.ratios(lambda pdf: \
                          fun_lessequal_errors(pdf, 1 + n_errors),
                          credibility=confidence, 
                          name="At most %d errors" % n_errors)
    marg_0 = prob.Marginal(0, name="$\\alpha$")
    marg_1 = prob.Marginal(1, name="$\\beta$")
    if plot:
        tp.Clf("qualitative")
        tp.PrePlot(num=2)
        tp.Pmfs([marg_0, marg_1])
        fname = os.path.join(outdir, "derived_%d_%s" % (n_errors, prior_kind))
        tp.Save(root=fname, formats=["pdf"],
                xlabel=\
                "values of $\\alpha, \\beta$ within %d\\%% credible intervals" \
                % confidence,
                ylabel="probability of at most %d errors" % n_errors)


def posterior_analysis(alpha_lims, beta_lims, confidence=95,
                       plot=True, CDF=False, print_values=True,
                       prior_kind="", outdir="testing/"):
    """Plot Weibulls for parameters in alpha_lims and beta_lims.

    alpha_lims: (low, mid, high level) of alpha parameter
    beta_lims: (low, mid, high level) of beta parameter
    confidence: percentage 0-100 for credible interval (for display)
    plot: if False, only compute statistics
    CDF: if True plot cdfs, otherwise plot pmfs approximating pdfs
    print_values: if True print actual values of alpha_lims and beta_lims in plot
                  if False print symbolic values
    prior_kind: a string identifying the prior (for filenames)
    outdir: where to write plots
    """
    def lhn(a, b):
        return "$\\alpha = %.1f, \\beta = %.1f$" % (a, b)
    a_low, a_opt, a_high = alpha_lims
    b_low, b_opt, b_high = beta_lims
    names = {(a_low, b_low): lhn(a_low, b_low) if print_values \
             else "$\\alpha_{%d}^l, \\beta_{%d}^l$" % (confidence, confidence),
             (a_opt, b_opt): lhn(a_opt, b_opt) if print_values \
             else "$\\widehat{\\alpha}, \\widehat{\\beta}$",
             (a_high, b_high): lhn(a_high, b_high) if print_values \
             else "$\\alpha_{%d}^h, \\beta_{%d}^h$" % (confidence, confidence)}
    xs = numpy.linspace(0,150,10000)
    pdfs = []
    pmfs = []
    cdfs = []
    for params in sorted(names.keys()):
        a, b = params
        pdf = collect.Weibull(a, b, zero=1, name=names[params])
        pdfs.append(pdf)
        pmf = pdf.MakePmf(xs, name=pdf.name)
        pmfs.append(pmf)
        if CDF:
            cdf = pmf.MakeCdf(name=pmf.name)
            cdfs.append(cdf)
            max_bugs = cdf.Value(1.0)
            eighty_bugs = cdf.Value(0.8)
            fraction = eighty_bugs/max_bugs
            print "alpha = %.2f, beta = %.2f: fraction of bugs in 80%%: %d" % \
                (a, b, round(100*fraction))
    if plot:
        tp.Clf("sequential")
        tp.PrePlot(num=len(pmfs))
        if CDF:
            tp.Cdfs(cdfs)
        else:
            tp.Pmfs(pmfs)
        fname = os.path.join(outdir, "posts_%s" % prior_kind)
        tp.Save(root=fname, formats=["pdf"],
                xlabel="\\# bugs", ylabel="probability/fraction")

        
def real_bugs_analysis(alpha, beta, d_m, m, prior_kind, confidence=95, plot=True, outdir=""):
    xs = range(0,201)
    Es = numpy.linspace(0.7, 0.95, 10)
    es = numpy.linspace(0.15, 0.5, 10)
    mt = collect.MetaTester(Es, es, alpha, beta, xs, zero=1)
    mt.Update(d_m)
    pars = mt.params_distro()
    nbugs = mt.nbugs_distro()
    nbugs.name = "$N_{%d}[n]$" % m
    fname = os.path.join(outdir, "credib_eE_M_%d_%s" % (m, prior_kind))
    nlow, nhigh = nbugs.CredibleInterval(confidence)
    nopt = nbugs.MaximumLikelihood()
    vnopt = nbugs.Prob(nopt)
    nmean = nbugs.Mean()
    nmedian = tb.Percentile(nbugs, 50)
    if plot:
        credibility_intervals(pars, fname, xlabel="$e$", ylabel="$E$")
        fig = tp.Clf()
        tp.PrePlot(num=1)
        tp.Pmf(nbugs)
        add_confidence(fig, nlow, nhigh, nopt, vnopt, confidence, "N")
        fname = os.path.join(outdir, "nbugs_M_%d_%s" % (m, prior_kind))
        tp.Save(root=fname, formats=["pdf"],
                xlabel="number $n$ of bugs in module $C_{%d}$" % m,
                ylabel="probability")
    return (nlow, nopt, nhigh, nmean, nmedian)


def testing_analysis(out_dir):
    original, mbc, datas, LOC, routines = collect.read_mbc(out_dir)
    for family in datas:
        init, init_mbc, suites = datas[family]
        derived_pmfs = {}
        alpha_name = "\\alpha"
        beta_name = "\\beta"
        for prior_kind in sorted(suites.keys()):
            derived_pmfs[prior_kind] = {}
            suite = suites[prior_kind]
            data = mbc
            suite.UpdateSet(data)
            alpha_lims, beta_lims = marginal_analysis(suite, prior_kind,
                                                      confidence=90, plot=True)
            alpha_opt, beta_opt = alpha_lims[1], beta_lims[1]
            posterior_analysis(alpha_lims, beta_lims, confidence=90, 
                               prior_kind=prior_kind, print_values=False,
                               plot=True, CDF=True)
            derived_analysis(suite, 0, prior_kind, confidence=90, plot=True)
            derived_analysis(suite, 5, prior_kind, confidence=90, plot=True)
            lows, opts, highs, means, medians = [], [], [], [], []
            per_r, per_loc = [], []
            for m in range(len(original)):
                d_m = original[m]
                nlow, nopt, nhigh, nmean, nmedian = real_bugs_analysis(alpha_opt,
                                                                       beta_opt,
                                                                       d_m, m + 1,
                                                                       prior_kind,
                                                                       confidence=90,
                                                                       plot=True,
                                                                       outdir=out_dir)
                lows.append(nlow)
                opts.append(nopt)
                highs.append(nhigh)
                means.append(nmean)
                medians.append(nmedian)
                per_r.append(nmedian/routines[m])
                per_loc.append(nmedian/LOC[m])
            confidence=90
            lows = ["$N_{%d}^l$" % confidence]  + ["%d" % round(v) for v in lows]
            opts = ["$\widehat{N}$"]            + ["%d" % round(v) for v in opts]
            highs = ["$N_{%d}^h$" % confidence] + ["%d" % round(v) for v in highs]
            means = ["$\mu(N)$"]                + ["%d" % round(v) for v in means]
            medians = ["$m(N)$"]                + ["%d" % round(v) for v in medians]
            per_r = ["$m(N)/\\text{\\#R}$"]     + ["%.2f" % v for v in per_r]
            per_loc = ["$m(N)/\\textsc{loc}$"]  + ["%.1E" % v for v in per_loc]
            rows = [means, medians, opts, lows, highs, per_loc, per_r]
            write_table(rows, out_dir + "/", "tab_nbugs_%s.tex" % prior_kind)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Bayesian analysis of three case studies.')
    parser.add_argument('--scratch', action='store_true', 
                        help='create partial results file from scratch even if it exists')
    parser.add_argument('analysis', action='store', nargs=1, choices=available_analyses, 
                        help='analysis to be performed')
    args = parser.parse_args()
    analysis = args.analysis[0]
    if analysis in ["rosetta", "ALL"]:
        print "Performing analysis of Rosetta Code data."
        out_dir = data_dirs["rosetta"]
        rosetta_analysis(out_dir, args.scratch)
    if analysis in ["agile", "ALL"]:
        print "Performing analysis of Agile vs. Structured data."
        out_dir = data_dirs["agile"]
        agile_analysis(out_dir)
    if analysis in ["testing", "ALL"]:
        print "Performing analysis of Random Testing data."
        out_dir = data_dirs["testing"]
        testing_analysis(out_dir)
